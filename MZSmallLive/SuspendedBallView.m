//
//  SuspendedBallView.m
//  MZSmallLive
//
//  Created by kingly on 16/6/16.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import "SuspendedBallView.h"

@implementation SuspendedBallView

+ (SuspendedBallView *)createView
{
    static dispatch_once_t token;
    static SuspendedBallView *ballView;
    dispatch_once(&token, ^{
        ballView = [[SuspendedBallView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
        [ballView configBallView];
    });
    return ballView;
}

+ (void)showBallView;
{

    [[UIApplication sharedApplication].keyWindow addSubview:[SuspendedBallView createView]];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:[SuspendedBallView createView]];
}

#pragma mark - private response

- (void)configBallView
{

    [self addSubview:self.ballView];

    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMenuImages:)];
    tapGes.numberOfTapsRequired = 1.0f;
    tapGes.numberOfTouchesRequired = 1.0f;
    [self addGestureRecognizer:tapGes];

    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragBallView:)];
    [self addGestureRecognizer:panGes];
    
    
}


- (void)dragBallView:(UIPanGestureRecognizer *)panGes
{
    CGPoint translation = [panGes translationInView:[UIApplication sharedApplication].keyWindow];
    CGPoint center = self.center;
    self.center = CGPointMake(center.x+translation.x, center.y+translation.y);
    [panGes setTranslation:CGPointMake(0, 0) inView:[UIApplication sharedApplication].keyWindow];
    if (panGes.state == UIGestureRecognizerStateBegan) {


    }
    else if (panGes.state == UIGestureRecognizerStateEnded)
    {

    }
}
#pragma mark - getter

- (UIImageView *)ballView
{
    if (!_ballView) {
        _ballView = [[UIImageView alloc]initWithFrame:self.frame];
        [_ballView setImage:[UIImage imageNamed:@"account_highlight"]];
    }
    return _ballView;
}

#pragma mark - 

- (void)showMenuImages:(UITapGestureRecognizer *)tapGes
{

    
}

@end
