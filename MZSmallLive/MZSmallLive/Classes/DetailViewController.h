//
//  DetailViewController.h
//  MZSmallLive
//
//  Created by kingly on 16/6/15.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailViewControllerDelegate;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) UIWindow *currWindow;

- (IBAction)onClickClose:(id)sender;
- (IBAction)onClickChangeSmall:(id)sender;

@property (nonatomic,weak) id<DetailViewControllerDelegate>mydelegate;

@end

/**
 * ToolBar点击协议
 */
@protocol DetailViewControllerDelegate <NSObject>

@optional


-(void)ChangeSmall;

@end
