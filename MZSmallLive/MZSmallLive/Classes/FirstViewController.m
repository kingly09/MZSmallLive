//
//  FirstViewController.m
//  LiBTabBarController
//
//  Created by kingly https://github.com/kingly09/LBinTabBarController  on 15/12/11.
//  Copyright © 2015年 kingly. All rights reserved.
//

#import "FirstViewController.h"
#import "DetailViewController.h"
#import "MZLiveWindow.h"

@interface FirstViewController (){

      UIWindow *_myWindow;

    MZLiveWindow *liveWindow;

}

@end

@implementation FirstViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    [self.navigationController.tabBarItem setBadgeValue:@"3"];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return YES;
    }
    return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

#pragma mark - Methods

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    [[cell textLabel] setText:[NSString stringWithFormat:@"%@ Controller Cell %@", self.title, @(indexPath.row)]];
}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell forIndexPath:indexPath];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

//    DetailViewController *detailView = [[DetailViewController alloc] init];
//    [self.navigationController pushViewController:detailView animated:YES];
    liveWindow.hidden = YES;
    liveWindow  = nil;
    liveWindow  =  [[MZLiveWindow alloc] initWithFrameView:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    liveWindow.hidden = NO;

}

-(void)creatMyWindow{


    //创建一个UIWindow
    _myWindow = [[UIWindow alloc] initWithFrame:CGRectMake(200, 0, 300,300)];
    _myWindow.windowLevel = UIWindowLevelAlert;
    _myWindow.backgroundColor = [UIColor blackColor];
    _myWindow.alpha = 0.7;
    _myWindow.hidden = NO;

    //添加手势
    UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] init];
    [gesture addTarget:self action:@selector(hideWindow:)];
    [_myWindow addGestureRecognizer:gesture];

    //在window上添加一个UILabel
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 20)];
    label.text = @"LABEL";
    [label setTextColor:[UIColor whiteColor]];
    [_myWindow addSubview:label];

    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragBallView:)];
    [_myWindow addGestureRecognizer:panGes];

}

- (void)hideWindow:(UIGestureRecognizer *)gesture {
    _myWindow.hidden = YES;
    _myWindow = nil;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    UIView *viewDemo = (UIView*)[self.view  viewWithTag:100];
    CGPoint point = [touch  locationInView:self.view];
    CGRect  frame = viewDemo.frame;
    frame.origin = point;
    viewDemo.frame = frame;
}

- (void)dragBallView:(UIPanGestureRecognizer *)panGes
{
    CGPoint translation = [panGes translationInView:[UIApplication sharedApplication].keyWindow];
    CGPoint center = _myWindow.center;
    _myWindow.center = CGPointMake(center.x+translation.x, center.y+translation.y);
    [panGes setTranslation:CGPointMake(0, 0) inView:[UIApplication sharedApplication].keyWindow];
    if (panGes.state == UIGestureRecognizerStateBegan) {

    }
    else if (panGes.state == UIGestureRecognizerStateEnded)
    {
    }
}



@end
