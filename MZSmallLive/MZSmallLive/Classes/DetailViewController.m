//
//  DetailViewController.m
//  MZSmallLive
//
//  Created by kingly on 16/6/15.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"详细01界面";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickClose:(id)sender {

    _currWindow.hidden = YES;
    _currWindow=nil;
}

- (IBAction)onClickChangeSmall:(id)sender {


    _currWindow.frame =  CGRectMake(0, 0, 300,300);
    self.view.frame = CGRectMake(0, 0, 300,300);

    if (_mydelegate && [_mydelegate respondsToSelector:@selector(ChangeSmall)]) {
        [_mydelegate ChangeSmall];
    }

}
@end
