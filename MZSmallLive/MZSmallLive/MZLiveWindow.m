//
//  MZLiveWindow.m
//  MZSmallLive
//
//  Created by kingly on 16/6/15.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import "MZLiveWindow.h"
#import "DetailViewController.h"

#define WIDTH self.frame.size.width
#define HEIGHT self.frame.size.height
#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height




UIButton *button[4];//button数组
CGPoint touchpoint;//触摸点的位置


#define WeakSelf       __weak typeof(self) weakSelf = self
#define StrongSelf     __strong typeof(weakSelf) strongSelf = weakSelf

@interface MZLiveWindow ()<DetailViewControllerDelegate>

@end

@implementation MZLiveWindow{


    UIButton *zoomInBtn;  //点击放大
    UIButton *zoomOutBtn; //点击缩小
    UIPanGestureRecognizer *panGess;
    UIView *_liveView; //直播视图

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}


-(id)initWithFrameView:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {


//        [self addCustomView];
        __weak MZLiveWindow *weakSelf = self;
        self.windowLevel = UIWindowLevelAlert;
        self.backgroundColor = [UIColor blackColor];
        DetailViewController *DetailView = [[DetailViewController alloc] init];
        DetailView.currWindow = weakSelf;
        DetailView.mydelegate = self;
        [self setRootViewController:DetailView];

    }
    return self;

}


/**
 *  添加自定义视图
 */
-(void)addCustomView{

    self.windowLevel = UIWindowLevelAlert;
    self.backgroundColor = [UIColor blackColor];
    self.alpha = 0.7;

    DetailViewController *DetailView = [[DetailViewController alloc] init];

    [self setRootViewController:DetailView];

    //添加手势
//    UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] init];
//    [gesture addTarget:self action:@selector(hideWindow:)];
//    [self addGestureRecognizer:gesture];

    _liveView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _liveView.backgroundColor = [UIColor redColor];
    [self addSubview:_liveView];

    //在_liveView上添加一个UILabel
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 20)];
    label.text = @"添加一个UILabel";
    [label setTextColor:[UIColor whiteColor]];
    _liveView.userInteractionEnabled  = YES;
    [_liveView addSubview:label];

    //在_liveView上添加一个UIButton

    //点击放大
    zoomInBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    zoomInBtn.frame = CGRectMake(10, 50, 80, 80);
    [zoomInBtn adjustsImageWhenHighlighted];
    [zoomInBtn adjustsImageWhenDisabled];
    zoomInBtn.imageView.contentMode = UIViewContentModeCenter;
    [zoomInBtn setTitle:@"点击放大" forState:UIControlStateNormal];
    zoomInBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    zoomInBtn.titleLabel.textColor = [UIColor whiteColor];
    [zoomInBtn addTarget:self action:@selector(onClickZoomInBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] init];
//    [gesture addTarget:self action:@selector(hideWindow:)];
//    [zoomInBtn addGestureRecognizer:gesture];
    [_liveView addSubview:zoomInBtn];

    //点击缩小
    zoomOutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    zoomOutBtn.frame = CGRectMake(self.bounds.size.width - 80, self.bounds.size.height - 100, 80, 80);
    [zoomOutBtn adjustsImageWhenHighlighted];
    [zoomOutBtn adjustsImageWhenDisabled];
    zoomOutBtn.imageView.contentMode = UIViewContentModeCenter;
    [zoomOutBtn setTitle:@"点击缩小" forState:UIControlStateNormal];
    zoomOutBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    zoomOutBtn.titleLabel.textColor = [UIColor whiteColor];
    [zoomOutBtn addTarget:self action:@selector(onClickZoomOutBtnBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_liveView addSubview:zoomOutBtn];

}

- (void)hideWindow:(UIGestureRecognizer *)gesture {
    self.hidden = YES;

}

- (void)dragBallView:(UIPanGestureRecognizer *)panGes
{
    CGPoint translation = [panGes translationInView:[UIApplication sharedApplication].keyWindow];
    CGPoint center = self.center;
    self.center = CGPointMake(center.x+translation.x, center.y+translation.y);
    [panGes setTranslation:CGPointMake(0, 0) inView:[UIApplication sharedApplication].keyWindow];
    if (panGes.state == UIGestureRecognizerStateBegan) {

    }
    else if (panGes.state == UIGestureRecognizerStateEnded)
    {
    }
}

#pragma mark - getter
//- (UIView *)liveView
//{
//    if (!_liveView) {
//        _liveView = [[UIImageView alloc]initWithFrame:self.bounds];
//        _liveView.backgroundColor = [UIColor redColor];
//    }
//    return _liveView;
//}

#pragma mark - 

-(void)onClickZoomInBtn:(UIButton *)sender{

    self.frame = CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT);
    _liveView.frame = CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT);
    if (panGess) {
        [panGess removeTarget:self action:@selector(dragBallView:)];
    }

}

-(void)onClickZoomOutBtnBtn:(UIButton *)sender{

    self.frame = CGRectMake(SCREEN_WIDTH - kSmallLiveViewWidth ,0, kSmallLiveViewWidth, kSmallLiveViewHeight);
    _liveView.frame = CGRectMake(0,0, kSmallLiveViewWidth, kSmallLiveViewHeight);
    //滑动手势
   }
#pragma mark - 变小
-(void)ChangeSmall{

    panGess = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(dragBallView:)];
    [self addGestureRecognizer:panGess];

}

@end
