//
//  MZLiveWindow.h
//  MZSmallLive
//
//  Created by kingly on 16/6/15.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define kSmallLiveViewWidth  (SCREEN_WIDTH  * 0.4)
#define kSmallLiveViewHeight (SCREEN_HEIGHT * 0.4)

@interface MZLiveWindow : UIWindow<UIGestureRecognizerDelegate>

-(id)initWithFrameView:(CGRect)frame;
/**
 * 注销MZLiveWindow
 **/
-(void)resignLiveWindow;

@end
