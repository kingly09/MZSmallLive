//
//  SuspendedBallView.h
//  MZSmallLive
//
//  Created by kingly on 16/6/16.
//  Copyright © 2016年 kingly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuspendedBallView : UIView<UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImageView *ballView;

+ (void)showBallView;

@end
